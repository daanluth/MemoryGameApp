# Memory Game
## Description
A small android app i made.<br/>
It asks the user to memorize a string of numbers, and then repeat the<br/>
string. The objective is to get the numbers right.<br/>
<br/>
the output APK is MemoryGame.apk<br/>
and the Android Studio Project / Source code is in MemoryGame.tar.gz<br/>
## Usage
Install and open the app, then tap "Tap to Start" to, well, start.<br/>
A number will appear, try to memorize it and type it in on the number<br/>
pad once it is gone and the buttons light up.<br/>
After you have entered the number, a response message will appear.<br/>
If you tap the response message, the next number will show up.<br/>
<br/>
If you get a number right, the game gets harder (longer numbers),<br/>
if you get it wrong, the game becomes easier (shorter numbers).<br/>
<br/>
You can open the stats/settings menu to find some stats, the current<br/>
difficulty, and a switch that activates a special mode..<br/>
